import React from 'react';
import { View, StyleSheet } from 'react-native';
import firebase from 'firebase/app';
import AppContainer from './Navigation';

firebase.initializeApp({
  apiKey: 'AIzaSyBQHalC7xe4ovzCnJUQJ8aZZAG3t9fGBMs',
  authDomain: 'eventcounter-58595.firebaseapp.com',
  databaseURL: 'https://eventcounter-58595.firebaseio.com',
  projectId: 'eventcounter-58595',
  storageBucket: 'eventcounter-58595.appspot.com',
  messagingSenderId: '849376025217'
});
const App = () => {
  return (
    <View style={styles.container}>
      <AppContainer />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF'
  }
});

export default App;
