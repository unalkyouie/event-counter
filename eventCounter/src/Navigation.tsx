import React from 'react';
import {
  createAppContainer,
  createSwitchNavigator,
  createStackNavigator
} from 'react-navigation';
import EventList from './screens/EventList';
import StartScreen from './screens/StartScreen';
import AddEvent from './screens/AddEvent';

const AppNavigator = createSwitchNavigator({
  start: StartScreen,
  events: EventList,
  add: AddEvent
});

const AppContainer = createAppContainer(AppNavigator);

export default AppContainer;
