import React, { useState, useEffect } from 'react';
import { StyleSheet, ScrollView, Dimensions, View } from 'react-native';

import Event from '../components/Event';

import firebase from '@firebase/app';
import '@firebase/database';
import LinearGradient from 'react-native-linear-gradient';

interface EventProps {
  eventKey: string;
  name: string;
  date: Date;
}

const List = () => {
  const [events, setEvents] = useState<EventProps[]>([]);
  useEffect(() => {
    try {
      firebase.database!()
        .ref('events/')
        .on('value', snapshot => {
          const snapshotValue = snapshot!.val();
          if (snapshotValue) {
            let data: EventProps[];
            data = [];
            Object.keys(snapshotValue).map(key => {
              data.push({ eventKey: key, ...snapshotValue[key] });
            });
            data.sort(
              (b, a) => new Date(b.date).valueOf() - new Date(a.date).valueOf()
            );

            setEvents([...data]);
          }
        });
    } catch (e) {
      // error reading value
    }
  }, [setEvents]);

  return (
    <ScrollView style={styles.scrollview} snapToEnd={true}>
      {events.map(event => (
        <Event name={event.name} date={event.date} eventKey={event.eventKey} />
      ))}
    </ScrollView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#000',
    alignSelf: 'stretch'
  },

  scrollview: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    opacity: 0.8,
    marginBottom: 100,
    marginTop: 40
  },
  gradient: {
    justifyContent: 'center',
    alignItems: 'center',
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
  }
});

export default List;
