import React, { useState } from 'react';
import { View, Text, StyleSheet } from 'react-native';

import '@firebase/database';

const Time = () => {
  const [time, setTime] = useState();
  const currentDate = new Date().toLocaleDateString();
  const currentTime = () => {
    setInterval(() => {
      let hour = new Date().getHours();
      let minutes = new Date().getMinutes();
      let seconds = new Date().getSeconds();
      let minutesString: string;
      let secondsString: string;

      if (minutes < 10) {
        minutesString = '0' + minutes;
      } else {
        minutesString = minutes.toString();
      }
      if (seconds < 10) {
        secondsString = '0' + seconds;
      } else {
        secondsString = seconds.toString();
      }
      if (hour > 12) {
        hour = hour - 12;
      }
      if (hour == 0) {
        hour = 12;
      }
      setTime(hour + ':' + minutesString + ':' + secondsString + ' ');
    }, 1000);
    return time;
  };

  return (
    <View style={styles.container}>
      <Text style={styles.text}>
        {currentDate} {currentTime()}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 10,
    flex: 1,
    overflow: 'visible',
    position: 'absolute',
    bottom: 10,
    left: 20
  },
  text: {
    fontSize: 18,
    padding: 10,
    color: '#fff',
    flex: 1,
    overflow: 'visible',
    position: 'absolute',
    bottom: 10
  }
});

export default Time;
