import React from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  ImageBackground
} from 'react-native';
import { NavigationScreenProp } from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/MaterialIcons';

import '@firebase/database';
import Time from '../components/Time';
import List from '../components/List';

interface EventListProps {
  navigation: NavigationScreenProp<any>;
}

const EventList = (props: EventListProps) => {
  return (
    <View style={styles.container}>
      <ImageBackground
        source={require('../assets/mountains.jpg')}
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          bottom: 0,
          width: '100%',
          height: '100%'
        }}
      >
        <LinearGradient colors={['#ffffff00', '#000']} style={styles.gradient}>
          <List />
        </LinearGradient>
        <Time />
        <TouchableOpacity
          onPress={() => props.navigation.navigate('add')}
          style={styles.addButton}
        >
          <Icon size={36} name="add" />
        </TouchableOpacity>
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#000',
    alignSelf: 'stretch'
  },

  button: {
    padding: 10,
    overflow: 'visible',
    position: 'absolute',
    bottom: 40,
    left: 20
  },
  gradient: {
    justifyContent: 'center',
    alignItems: 'center',
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
  },
  addButton: {
    borderRadius: 100,
    backgroundColor: '#FCACB1',
    width: 70,
    height: 70,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'flex-end',
    right: 30,
    position: 'absolute',
    bottom: 20,
    overflow: 'visible'
  }
});

export default EventList;
