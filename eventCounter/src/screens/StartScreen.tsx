import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  Dimensions,
  TouchableOpacity,
  TouchableHighlight,
  SwipeableListView
} from 'react-native';
import { NavigationScreenProp } from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';

interface StartScreenProps {
  navigation: NavigationScreenProp<any>;
}

const StartScreen = (props: StartScreenProps) => {
  return (
    <TouchableHighlight onPress={() => props.navigation.navigate('events')}>
      <View style={styles.container}>
        <ImageBackground
          source={require('../assets/mountains.jpg')}
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            bottom: 0,
            width: '100%',
            height: '100%'
          }}
        >
          <LinearGradient
            colors={['#ffffff00', '#000']}
            style={styles.gradient}
          ></LinearGradient>
        </ImageBackground>
      </View>
    </TouchableHighlight>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  gradient: {
    justifyContent: 'center',
    alignItems: 'center',
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
  }
});

export default StartScreen;
