import React from 'react';
import { shallow, mount } from 'enzyme';
import AddEvent from '../src/screens/AddEvent';
import { createNavigationProps } from './createNavigationProps';

const properties = {
  name: 'Event1',
  date: new Date(),
  eventKey: 'event1'
};

describe('AddEvent', () => {
  it('should contain BackgroundImage', () => {
    const wrapper = mount(
      <AddEvent
        name={properties.name}
        date={properties.date}
        eventKey={properties.eventKey}
        {...createNavigationProps({})}
      />
    );
    expect(wrapper.find('BackgroundImage')).toHaveLength(1);
    wrapper.unmount();
  });
  it('should contain LinearGradient', () => {
    const wrapper = mount(
      <AddEvent
        name={properties.name}
        date={properties.date}
        eventKey={properties.eventKey}
        {...createNavigationProps({})}
      />
    );
    expect(wrapper.find('LinearGradient')).toHaveLength(1);
    wrapper.unmount();
  });
  it('should contain 2 Text', () => {
    const wrapper = mount(
      <AddEvent
        name={properties.name}
        date={properties.date}
        eventKey={properties.eventKey}
        {...createNavigationProps({})}
      />
    );
    expect(wrapper.find('Text')).toHaveLength(2);
    wrapper.unmount();
  });
  it('should contain 3 TouchableOpacity', () => {
    const wrapper = mount(
      <AddEvent
        name={properties.name}
        date={properties.date}
        eventKey={properties.eventKey}
        {...createNavigationProps({})}
      />
    );
    expect(wrapper.find('TouchableOpacity')).toHaveLength(3);
    wrapper.unmount();
  });
  it('should contain TextInput', () => {
    const wrapper = mount(
      <AddEvent
        name={properties.name}
        date={properties.date}
        eventKey={properties.eventKey}
        {...createNavigationProps({})}
      />
    );
    expect(wrapper.find('TextInput')).toHaveLength(1);
    wrapper.unmount();
  });
  it('should contain DateTimePicker', () => {
    const wrapper = mount(
      <AddEvent
        name={properties.name}
        date={properties.date}
        eventKey={properties.eventKey}
        {...createNavigationProps({})}
      />
    );
    expect(wrapper.find('DateTimePicker')).toHaveLength(1);
    wrapper.unmount();
  });
  it('should contain TouchableOpacity Icon calendar', () => {
    const wrapper = mount(
      <AddEvent
        name={properties.name}
        date={properties.date}
        eventKey={properties.eventKey}
        {...createNavigationProps({})}
      />
    );
    expect(
      wrapper
        .find('TouchableOpacity')
        .first()
        .contains('Icon')
    ).toBe(true);
    wrapper.unmount();
  });
});
