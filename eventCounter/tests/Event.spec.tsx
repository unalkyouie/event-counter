import React from 'react';
import { shallow } from 'enzyme';
import Event from '../src/components/Event';
import firebase from '@firebase/app';
import wait from 'waait';
firebase.initializeApp({
  apiKey: 'AIzaSyBQHalC7xe4ovzCnJUQJ8aZZAG3t9fGBMs',
  authDomain: 'eventcounter-58595.firebaseapp.com',
  databaseURL: 'https://eventcounter-58595.firebaseio.com',
  projectId: 'eventcounter-58595',
  storageBucket: 'eventcounter-58595.appspot.com',
  messagingSenderId: '849376025217'
});
const properties = {
  name: 'Event1',
  date: new Date(),
  eventKey: 'event1'
};

describe('Event', () => {
  it('Should contian 3 <Text></Text>', async () => {
    await wait(0);
    const newDate = new Date();
    const wrapper = shallow(
      <Event name="name" date={newDate} eventKey="key" />
    );
    expect(wrapper.find('Text')).toHaveLength(3);
  });
  // it('Should render an event name <Text/>', async () => {
  //   const wrapper = shallow(
  //     <Event
  //       name={properties.name}
  //       date={properties.date}
  //       eventKey={properties.eventKey}
  //     />
  //   );
  //   await wait(0);

  //   const nameSnapshot = await firebase.database!()
  //     .ref('events/' + properties.name)
  //     .once('value');
  //   const event = nameSnapshot.val().name;
  //   expect(
  //     shallow(wrapper.find('Text').get(0))
  //       .render()
  //       .text()
  //   ).toEqual(event);
  // });
  it('should contian 2 TouchableOpacity', () => {
    const newDate = new Date();
    const wrapper = shallow(
      <Event name="name" date={newDate} eventKey="key" />
    );
    expect(wrapper.find('TouchableOpacity')).toHaveLength(2);
  });
  it('Both TouchableOpacity should contain icons', () => {
    const newDate = new Date();
    const wrapper = shallow(
      <Event name="name" date={newDate} eventKey="key" />
    );
    expect(
      wrapper
        .find('TouchableOpacity')
        .first()
        .find('Icon')
    ).toHaveLength(1);
    expect(
      wrapper
        .find('TouchableOpacity')
        .last()
        .find('Icon')
    ).toHaveLength(1);
  });
  //   it('Both TouchableOpacity should call a function', () => {
  //     const method = jest.fn();
  //     const newDate = new Date();
  //     const wrapper = shallow(
  //       <Event name="name" date={newDate} eventKey="key" />
  //     );

  //     wrapper
  //       .find('TouchableOpacity')
  //       .first()
  //       .simulate('press');
  //     expect(method).toBeCalledTimes(1);
  //     wrapper
  //       .find('TouchableOpacity')
  //       .last()
  //       .simulate('press');
  //     expect(method).toBeCalledTimes(1);
  //   });
});
