import React from 'react';
import { shallow, mount } from 'enzyme';
import StartScreen from '../src/screens/StartScreen';
import { createNavigationProps } from './createNavigationProps';
import wait from 'waait';
import './eventEmitterMock';

jest.mock('NativeEventEmitter');

describe('StartScreen', () => {
  it('should contain TouchableHighlight', () => {
    const wrapper = shallow(<StartScreen {...createNavigationProps({})} />);
    expect(wrapper.find('TouchableHighlight')).toHaveLength(1);
  });
  it('should contain LinearGradient', () => {
    const wrapper = shallow(<StartScreen {...createNavigationProps({})} />);
    expect(wrapper.find('LinearGradient')).toHaveLength(1);
  });
  it('should contain ImageBackground', () => {
    const wrapper = shallow(<StartScreen {...createNavigationProps({})} />);
    expect(wrapper.find('ImageBackground')).toHaveLength(1);
  });
  it('should call navigate method after clicking on TouchableHighlith', async () => {
    const startScreenProps = createNavigationProps({});
    const wrapper = mount(<StartScreen {...createNavigationProps({})} />);
    await wait(1000);
    expect(startScreenProps.navigation.navigate).toBeCalledWith('events');
  });
});
