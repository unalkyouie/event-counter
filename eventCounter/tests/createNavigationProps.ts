import { NavigationScreenProp } from 'react-navigation';

const createNavigationProps = (
  props: object
): { navigation: NavigationScreenProp<any>; [key: string]: any } => ({
  navigation: {
    navigate: jest.fn(),
    state: jest.fn(),
    dispatch: jest.fn(),
    goBack: jest.fn(),
    dismiss: jest.fn(),
    openDrawer: jest.fn(),
    closeDrawer: jest.fn(),
    toggleDrawer: jest.fn(),
    getParam: jest.fn(),
    setParams: jest.fn(),
    addListener: jest.fn(),
    push: jest.fn(),
    replace: jest.fn(),
    pop: jest.fn(),
    popToTop: jest.fn(),
    isFocused: jest.fn(),
    dangerouslyGetParent: jest.fn(),
    reset: jest.fn(),
    isFirstRouteInParent: jest.fn()
  },
  ...props
});

export { createNavigationProps };
